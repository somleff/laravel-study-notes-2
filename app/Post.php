<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Post extends Model
{
    //1 способ: все указаные поля будут обработанны формой
    //protected $fillable = ['title', 'body'];

    //2 способ: все указаные поля НЕ будут обработанны формой, а не указанные будут
    //protected $guarded = ['user_id'];

    //пустой массив означает, что все поля будут обработанны формой
    //protected $guarded = [];
    //
    //3способ
    //вместо того чтобы писать protected $guarded = []; в каждом class, мы можем создать родительский class и наследовать его.
    //
    //Создаем модель Model. Эту модель наследуют все class
    //
    //В строке use Illuminate\Database\Eloquent\Model; класса Model
    //добавляем use Illuminate\Database\Eloquent\Model as Eloquent;
    //
    //удаляем use Illuminate\Database\Eloquent\Model; в class Post, ведь он наследует модель Model
    //теперь все Eloquent модели созданные в будущем будут наследовать условие protected $guarded = [];

    public function comments()
    {
        //return $this->hasMany(Comment::class)
        //
        // выводим последний добавленный комент
        return $this->hasMany(Comment::class)->latest();
    }


    public function user()  //$post->user or $comment->post->user
    {
        return $this->belongsTo(User::class);
    }


    //  Самый простой вариант добавления комментов/ Работает со стройкой в Post.php
    //  Все как прежде лишь добавляем user_id в compact
    public function addComment($body)
    {
        if (! $user_id = auth()->id()){
            return back()->withErrors([
                'message' => 'For sending comments please register or login'
            ]);
        }
        $this->comments()->create(compact('body', 'user_id'));

        // Comment::create([
        //     'body' => $body,
        //     'post_id' => $this->id
        // ]);
    }


    public function scopeFilter( $query, $filters )
    {
        if ($month = $filters['month']){
            $query->whereMonth('created_at', Carbon::parse($month)->month);
        }

        if ($year = $filters['year']){
            $query->whereYear('created_at', $year);
        }
    }


    //  Чтобы метод archives работал на всех страницах в AppServiseProvider прописиваем  в методе boot
    public static function archives()
    {
        return static::selectRaw ('year(created_at) year, monthname(created_at) month, count(*) published')
                    ->groupBy('year','month')
                    ->orderByRaw('min(created_at) desc')
                    ->get()
                    ->toArray();
    }


    public function tags()
    {
        //  Any post may have many tags
        //  Any tags may be applied to many posts
        return $this->belongsToMany(Tag::class);
    }
}