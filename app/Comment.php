<?php

namespace App;

//  Удаляем эту строку и наследуем нашу собственную модель Model
//use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //  Через коменты находим посты. $comment->post;
    public function post()
    {
        return $this->belongsTo(Post::class);
    }


    public function user()  //$comments->user->name
    {
        return $this->belongsTo(User::class);
    }
}
