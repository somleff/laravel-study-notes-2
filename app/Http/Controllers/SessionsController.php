<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionsController extends Controller
{
    public function __construct()   //Для гостей с паролем
    {
        $this->middleware('guest', ['except' => 'destroy']);
    }


    public function create()
    {
        if (auth()->attempt( request(['email', 'password']))){
            return back();
        }
        return view ('sessions.create');
    }


    public function store()
    {
        //  Пытаемся идентифицировать юзера.
        //  If not redirect back.
        //  If so, sign them in
       if (! auth()->attempt( request (['email', 'password'] ))){
            return back()->withErrors([
                'message' => 'Please check your credentials and try again.'
            ]);
       }
       //   Redirect to the home page
       return redirect()->home();
    }


    public function destroy()
    {
        auth()->logout();
        return redirect()->home();
    }
}
