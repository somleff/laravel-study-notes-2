<?php

namespace App\Http\Controllers;

//  Удаляем. В данном примере Request не используется.
//use Illuminate\Http\Request;
use App\Post;
use Carbon\Carbon;

class PostsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }


    public function index()
    {
        //  Выводим посты от последнего созданного к первому. Все варианты подходят
        //  $posts = Post::orderBy('created_at', 'asc')->get();
        //  $posts = Post::latest()->get();
        //
        //  Sidebar archive. Прописываем ссылки на посты созданные в конкретном месяце и году. Длинный спопоб.
        //
        // $posts = Post::latest();
        //
        // if ($month = request('month')){
        //     $posts->whereMonth('created_at', Carbon::parse($month)->month);
        // }
        //
        // if ($year = request('year')){
        //     $posts->whereYear('created_at', $year);
        // }
        //
        // $posts = $posts->get();
        //
        //  Короткий способ. Копируем ifы в метод scopeFilter() в Post.php
        //

        $posts = Post::latest()
            ->filter (request (['month', 'year']))
            ->get();

        return view('posts.index', compact('posts'));
    }


    public function show( Post $post )
    {
        return view('posts.show', compact('post'));
    }


    public function create()
    {
        return view('posts.create');
    }


    public function store()
    {
        //1вариант
        // //Creating a new post using the request data
        // $post = new Post;

        // $post->title = request('title');
        // $post->body = request('body');

        // //Save it to the database
        // $post->save();

        //2вариант(чтобы он работал безопастно в модели Post нужно создать fillable поля для вставляемых значений)
        // Post::create([
        //     'title' => request('title'),
        //     'body' => request('body')
        // ]);

        //валидация формы
        $this->validate(request(), [
            'title' => 'required',
            'body'  => 'required'
        ]);

        //  3вариант/ Публиковать могут все
        //Post::create(request(['title', 'body']));

        //  Публикуют только зареганные юзеры.
        auth()->user()->publish(
            new Post(request(['title', 'body']))
        );

        //  Flash message
        session()->flash('message', 'Your post has now been published.');

        //And then redirect to the home page
        return redirect('/');
    }
}