<?php

namespace App\Http\Controllers;

//use Mail;
//use App\User;
//use Illuminate\Support\Facades\Hash;
//use App\Mail\Welcome;
use Illuminate\Http\Request;
use App\Http\Requests\RegistrationRequest;


class RegistrationsController extends Controller
{
    public function create()
    {
        return view('registrations.create');
    }


    public function store(RegistrationRequest $request)
    {
        $request->persist();

        session()->flash('message', 'Thanks so for signing up!');

        //  This validate remove to the RegistrationRequest.php
        //  Validate the form
        // $this->validate(request(), [
        //     'name' => 'required',
        //     'email' => 'required|unique:users|email',
        //     'password' => 'required|min:3|confirmed'
        // ]);


        //  Create move to RegistrationRequest.php
        // //  Create and save the user
        // $user = User::create([
        //     'name'     => request('name'),
        //     'email'    => request('email'),
        //     'password' => Hash::make(request('password'))
        // ]);

        // //  Sign them in
        // auth()->login($user);

        // //  Send email welcome
        // Mail::to($user)->send(new Welcome($user));

        //Redirect to the home page
        return redirect()->home();
    }
}
