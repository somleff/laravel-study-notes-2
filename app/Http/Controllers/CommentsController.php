<?php

namespace App\Http\Controllers;

use App\Post;
use App\Comment;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    //  Оставляют коменты только зареганные юзеры. User.php содержит продолжение этого кода
    // public function store(Request $request, Post $post)
    // {
    //     $this->validate($request, ['body' => 'required|min:3']);
    //     auth()->user()->commentsOn($post, $request->body);
    //     return back();
    // }


    //  Оставляет коменты кто угодно. При регистрации с этим кодом начинаются проблемы
    //
    // public function store(Post $post)
    // {
    //     $this->validate(request(), ['body' => 'required|min:5']);
    //     $post->addComment(request('body'));
    //     return back();
    // }

    //  Самый простой вариант добавления комментов/ Работает со стройкой в Post.php
    public function store(Post $post)
    {
        $this->validate(request(), ['body' => 'required|min:3']);
        $post->addComment(request('body'), $post);
        return back();
    }
}
