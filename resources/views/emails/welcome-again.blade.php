@component('mail::message')
# Introduction

Thanks so much for registration!

@component('mail::button', ['url' => 'http://larastudy2.dev'])
Start Browsing
@endcomponent

@component('mail::panel', ['url' => ''])
Lorem ipsum dolar sit amet.
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
