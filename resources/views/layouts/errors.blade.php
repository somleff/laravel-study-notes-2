<!-- выводим ошибку о том, что поля формы не заполнены -->
<!-- we can use ($errors->any()) instead of (count($errors))-->
@if(count($errors))
<div class="form-group">
  <div class="alert alert-danger">
    <ul>
      @foreach($errors->all() as $error)
        <li>{{$error}}</li>
      @endforeach
    </ul>
  </div>
</div>
@endif